package estoque;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.jbehave.core.configuration.Configuration;
import org.jbehave.core.configuration.Keywords;
import org.jbehave.core.configuration.MostUsefulConfiguration;
import org.jbehave.core.i18n.LocalizedKeywords;
import org.jbehave.core.io.LoadFromClasspath;
import org.jbehave.core.junit.JUnitStories;
import org.jbehave.core.parsers.RegexStoryParser;
import org.jbehave.core.reporters.StoryReporterBuilder;
import org.jbehave.core.reporters.StoryReporterBuilder.Format;
import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;

public class EstoqueTeste extends JUnitStories {
	
	@SuppressWarnings("deprecation")
	public Configuration configuration() {

		Keywords keywords = new LocalizedKeywords(new Locale("pt"));

		return new MostUsefulConfiguration().useKeywords(keywords).useStoryParser(new RegexStoryParser(keywords))
				.useStoryLoader(new LoadFromClasspath(this.getClass())).useStoryReporterBuilder(
						new StoryReporterBuilder().withDefaultFormats().withFormats(Format.CONSOLE, Format.HTML));
	}

	@Override
	public InjectableStepsFactory stepsFactory() {
		return new InstanceStepsFactory(configuration(), new EstoqueSteps());
	}

	@Override
	protected List<String> storyPaths() {
		return Arrays.asList("estoque/estoque.story");
	}
}
