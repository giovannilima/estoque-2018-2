package estoque;

import static org.junit.Assert.assertEquals;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.steps.Steps;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class EstoqueSteps{
	
	static WebDriver driverChrome;
	static WebDriver driverFirefox;

	@Given("estou na lista de compras")
	public void abrirListaComplas() {
		System.setProperty("webdriver.chrome.driver", "C:/chromedriver/chromedriver.exe");
		System.setProperty("webdriver.gecko.driver", "C:/geckodriver/geckodriver.exe");
		//driverFirefox = new FirefoxDriver();
		//driverFirefox.get("file:///C:/Users/100942064/git/estoque-2018-2/estoque/src/main/webapp/lista-compras.html");

		 driverChrome = new ChromeDriver();
		 driverChrome.get("file:///C:/Users/100942064/git/estoque-2018-2/estoque/src/main/webapp/lista-compras.html");
	}

	@When("seleciono o produto $nome")
	public void selecionarProduto(String nome) {
		WebElement produto = driverChrome.findElement(By.id("produto"));
		produto.sendKeys(nome);
	}

	@When("informo a quantidade $quantidade")
	public void informarQuantidade(String quantidade) {
		WebElement inputQuantidade = driverChrome.findElement(By.id("quantidade"));
		inputQuantidade.sendKeys(quantidade);
	}
	
	@When("informo o valor unitário $reais reais")
	public void informarValor(String reais) {
		WebElement inputValor = driverChrome.findElement(By.id("valorUnitario"));
		inputValor.sendKeys(reais);
	}
	
	@When("E confirmo a compra")
	public void confirmarCompra() {
		WebElement button = driverChrome.findElement(By.id("calcularBtn"));
		button.click();
	}
	
	@Then("terei de pagar $reais reais")
	public void pagar(String reais) {
		WebElement result = driverChrome.findElement(By.id("valorTotal"));

		Assert.assertEquals(reais, result.getAttribute("value"));
	}

}
